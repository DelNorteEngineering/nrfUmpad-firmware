/* Copyright (c) 2014 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */
// Board/nrf6310/ble/ble_app_hrs_rtx/main.c
/**
 *
 * @brief Heart Rate Service Sample Application with RTX main file.
 *
 * This file contains the source code for a sample application using RTX and the
 * Heart Rate service (and also Battery and Device Information services).
 * This application uses the @ref srvlib_conn_params module.
 */

#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "app_error.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_bas.h"
#include "ble_dis.h"
#include "ble_hids.h"
#include "ble_conn_params.h"
#include "boards.h"
#include "softdevice_handler.h"
#include "app_timer.h"
#include "device_manager.h"
#include "pstorage.h"
#include "app_trace.h"
#include "bsp.h"
#include "bsp_btn_ble.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

#include "parameters.h"
#include "led_driver.h"
#include "key_scan.h"

static uint16_t m_conn_handle = BLE_CONN_HANDLE_INVALID; /**< Handle of the current connection. */
static ble_bas_t m_bas; /**< Structure used to identify the battery service. */
static ble_hids_t m_hids;

static dm_application_instance_t m_app_handle; /**< Application identifier allocated by device manager. */
static dm_handle_t m_bonded_peer_handle; /**< Device reference handle to the current bonded central. */

static bool m_in_boot_mode = false; /**< Current protocol mode. */
static bool m_caps_on = false; /**< Variable to indicate if Caps Lock is turned on. */
// TODO probably make a cooler data structure for keys/modifiers

static ble_uuid_t m_adv_uuids[] = /**< Universally unique service identifiers. */
{ { BLE_UUID_HUMAN_INTERFACE_DEVICE_SERVICE, BLE_UUID_TYPE_BLE }, {
BLE_UUID_BATTERY_SERVICE, BLE_UUID_TYPE_BLE }, {
BLE_UUID_DEVICE_INFORMATION_SERVICE, BLE_UUID_TYPE_BLE } };

static TimerHandle_t m_battery_timer; /**< Definition of battery timer. */

static SemaphoreHandle_t m_ble_event_ready; /**< Semaphore raised if there is a new event to be processed in the BLE thread. */

static TaskHandle_t m_ble_stack_thread; /**< Definition of BLE stack thread. */

// prototypes
static void hids_init();

/**@brief Callback function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in]   line_num   Line number of the failing ASSERT call.
 * @param[in]   file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name) {
  app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

/**@brief Function for performing battery measurement and updating the Battery Level characteristic
 *        in Battery Service.
 */
static void battery_level_update(void) {
  uint32_t err_code;
  uint8_t battery_level;

  battery_level = (uint8_t) 0xFF;
  // TODO ADC read and scale

  err_code = ble_bas_battery_level_update(&m_bas, battery_level);
  if ((err_code != NRF_SUCCESS) && (err_code != NRF_ERROR_INVALID_STATE)
      && (err_code != BLE_ERROR_NO_TX_PACKETS)
      && (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING)) {
    APP_ERROR_HANDLER(err_code);
  }
}

/**@brief Function for handling the Battery measurement timer time-out.
 *
 * @details This function will be called each time the battery level measurement timer expires.
 *
 * @param[in] xTimer Handler to the timer that called this function.
 *                   You may get identifier given to the function xTimerCreate using pvTimerGetTimerID.
 */
static void battery_level_meas_timeout_handler(TimerHandle_t xTimer) {
  // TODO move battery measurement to another C file
  UNUSED_PARAMETER(xTimer);
  battery_level_update();
}

/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module. This creates and starts application timers.
 */
static void timers_init(void) {
  // Initialize timer module.
  APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);

  // Create timers.
  m_battery_timer = xTimerCreate("BATT", BATTERY_LEVEL_MEAS_INTERVAL, pdTRUE,
  NULL, battery_level_meas_timeout_handler);

  /* Error checking */
  if ((NULL == m_battery_timer)) {
    APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
  }
}

/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
static void gap_params_init(void) {
  uint32_t err_code;
  ble_gap_conn_params_t gap_conn_params;
  ble_gap_conn_sec_mode_t sec_mode;

  BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

  err_code = sd_ble_gap_device_name_set(&sec_mode,
      (const uint8_t *) DEVICE_NAME, strlen(DEVICE_NAME));
  APP_ERROR_CHECK(err_code);

  err_code = sd_ble_gap_appearance_set(
  BLE_APPEARANCE_HEART_RATE_SENSOR_HEART_RATE_BELT);
  APP_ERROR_CHECK(err_code);

  memset(&gap_conn_params, 0, sizeof(gap_conn_params));

  gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
  gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
  gap_conn_params.slave_latency = SLAVE_LATENCY;
  gap_conn_params.conn_sup_timeout = CONN_SUP_TIMEOUT;

  err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
  APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing services that will be used by the application.
 *
 * @details Initialize the Heart Rate, Battery and Device Information services.
 */
static void services_init(void) {
  uint32_t err_code;
  ble_bas_init_t bas_init;
  ble_dis_init_t dis_init;

  // Initialize Battery Service.
  memset(&bas_init, 0, sizeof(bas_init));

  // Here the sec level for the Battery Service can be changed/increased.
  BLE_GAP_CONN_SEC_MODE_SET_OPEN(
      &bas_init.battery_level_char_attr_md.cccd_write_perm);
  BLE_GAP_CONN_SEC_MODE_SET_OPEN(
      &bas_init.battery_level_char_attr_md.read_perm);
  BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(
      &bas_init.battery_level_char_attr_md.write_perm);

  BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bas_init.battery_level_report_read_perm);

  bas_init.evt_handler = NULL;
  bas_init.support_notification = true;
  bas_init.p_report_ref = NULL;
  bas_init.initial_batt_level = 100;

  err_code = ble_bas_init(&m_bas, &bas_init);
  APP_ERROR_CHECK(err_code);

  // Initialize Device Information Service.
  memset(&dis_init, 0, sizeof(dis_init));

  ble_srv_ascii_to_utf8(&dis_init.manufact_name_str,
      (char *) MANUFACTURER_NAME);

  BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dis_init.dis_attr_md.read_perm);
  BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&dis_init.dis_attr_md.write_perm);

  err_code = ble_dis_init(&dis_init);
  APP_ERROR_CHECK(err_code);

  // Initialize HID service
  hids_init();
}

/**@brief Function for starting application timers.
 */
static void application_timers_start(void) {
  // Start application timers.
  if (pdPASS != xTimerStart(m_battery_timer, OSTIMER_WAIT_FOR_QUEUE)) {
    APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
  }
}

/**@brief Function for handling the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module which
 *          are passed to the application.
 *          @note All this function does is to disconnect. This could have been done by simply
 *                setting the disconnect_on_fail config parameter, but instead we use the event
 *                handler mechanism to demonstrate its use.
 *
 * @param[in]   p_evt   Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt) {
  uint32_t err_code;

  if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED) {
    err_code = sd_ble_gap_disconnect(m_conn_handle,
    BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
    APP_ERROR_CHECK(err_code);
  }
}

/**@brief Function for handling an nrf error code.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void error_handler(uint32_t nrf_error) {
  APP_ERROR_HANDLER(nrf_error);
}

/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void) {
  uint32_t err_code;
  ble_conn_params_init_t cp_init;

  memset(&cp_init, 0, sizeof(cp_init));

  cp_init.p_conn_params = NULL;
  cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
  cp_init.next_conn_params_update_delay = NEXT_CONN_PARAMS_UPDATE_DELAY;
  cp_init.max_conn_params_update_count = MAX_CONN_PARAMS_UPDATE_COUNT;
  cp_init.disconnect_on_fail = false;
  cp_init.evt_handler = on_conn_params_evt;
  cp_init.error_handler = error_handler;

  err_code = ble_conn_params_init(&cp_init);
  APP_ERROR_CHECK(err_code);
}

/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
static void sleep_mode_enter(void) {
  uint32_t err_code = bsp_indication_set(BSP_INDICATE_IDLE);
  APP_ERROR_CHECK(err_code);

  // Prepare wakeup buttons.
  err_code = bsp_btn_ble_sleep_mode_prepare();
  APP_ERROR_CHECK(err_code);

  // Go to system-off mode (this function will not return; wakeup will cause a reset).
  err_code = sd_power_system_off();
  APP_ERROR_CHECK(err_code);
}

/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt) {
  uint32_t err_code;

  switch (ble_adv_evt) {
  case BLE_ADV_EVT_FAST:
    err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
    APP_ERROR_CHECK(err_code);
    break;

  case BLE_ADV_EVT_IDLE:
    sleep_mode_enter();
    break;

  default:
    break;
  }
}

/**@brief Function for receiving the Application's BLE Stack events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 */
static void on_ble_evt(ble_evt_t * p_ble_evt) {
  uint32_t err_code;

  switch (p_ble_evt->header.evt_id) {
  case BLE_GAP_EVT_CONNECTED:

    err_code = bsp_indication_set(BSP_INDICATE_CONNECTED);
    APP_ERROR_CHECK(err_code);
    m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
    break;

  case BLE_GAP_EVT_DISCONNECTED:
    m_conn_handle = BLE_CONN_HANDLE_INVALID;
    break;

  default:
    // No implementation needed.
    break;
  }
}

/**@brief Function for dispatching a BLE stack event to all modules with a BLE stack event handler.
 *
 * @details This function is called from the BLE Stack event interrupt handler after a BLE stack
 *          event has been received.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 */
static void ble_evt_dispatch(ble_evt_t * p_ble_evt) {
  dm_ble_evt_handler(p_ble_evt);
  ble_bas_on_ble_evt(&m_bas, p_ble_evt);
  ble_conn_params_on_ble_evt(p_ble_evt);
  bsp_btn_ble_on_ble_evt(p_ble_evt);
  on_ble_evt(p_ble_evt);
  ble_advertising_on_ble_evt(p_ble_evt);
}

/**@brief Function for dispatching a system event to interested modules.
 *
 * @details This function is called from the System event interrupt handler after a system
 *          event has been received.
 *
 * @param[in]   sys_evt   System stack event.
 */
static void sys_evt_dispatch(uint32_t sys_evt) {
  pstorage_sys_event_handler(sys_evt);
  ble_advertising_on_sys_evt(sys_evt);
}

/**
 * @brief Event handler for new BLE events
 *
 * This function is called from the SoftDevice handler.
 * It is called from interrupt level.
 *
 * @return The returned value is checked in the softdevice_handler module,
 *         using the APP_ERROR_CHECK macro.
 */
static uint32_t ble_new_event_handler(void) {
  BaseType_t yield_req = pdFALSE;
  // The returned value may be safely ignored, if error is returned it only means that
  // the semaphore is already given (raised).
  UNUSED_VARIABLE(xSemaphoreGiveFromISR(m_ble_event_ready, &yield_req));
  portYIELD_FROM_ISR(yield_req);
  return NRF_SUCCESS;
}

/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void) {
  uint32_t err_code;

  nrf_clock_lf_cfg_t clock_lf_cfg = NRF_CLOCK_LFCLKSRC;

  // Initialize the SoftDevice handler module.
  SOFTDEVICE_HANDLER_INIT(&clock_lf_cfg, ble_new_event_handler);

  ble_enable_params_t ble_enable_params;
  err_code = softdevice_enable_get_default_config(CENTRAL_LINK_COUNT,
  PERIPHERAL_LINK_COUNT, &ble_enable_params);
  APP_ERROR_CHECK(err_code);

  //Check the ram settings against the used number of links
  CHECK_RAM_START_ADDR(CENTRAL_LINK_COUNT, PERIPHERAL_LINK_COUNT);

  // Enable BLE stack.
  err_code = softdevice_enable(&ble_enable_params);
  APP_ERROR_CHECK(err_code);

  // Register with the SoftDevice handler module for BLE events.
  err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch);
  APP_ERROR_CHECK(err_code);

  // Register with the SoftDevice handler module for BLE events.
  err_code = softdevice_sys_evt_handler_set(sys_evt_dispatch);
  APP_ERROR_CHECK(err_code);
}

/**@brief Function for handling events from the BSP module.
 *
 * @param[in]   event   Event generated by button press.
 */
static void bsp_event_handler(bsp_event_t event) {
  uint32_t err_code;
  switch (event) {
  case BSP_EVENT_SLEEP:
    sleep_mode_enter();
    break;

  case BSP_EVENT_DISCONNECT:
    err_code = sd_ble_gap_disconnect(m_conn_handle,
    BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
    if (err_code != NRF_ERROR_INVALID_STATE) {
      APP_ERROR_CHECK(err_code);
    }
    break;

  case BSP_EVENT_WHITELIST_OFF:
    err_code = ble_advertising_restart_without_whitelist();
    if (err_code != NRF_ERROR_INVALID_STATE) {
      APP_ERROR_CHECK(err_code);
    }
    break;

  default:
    break;
  }
}

/**@brief Function for handling the Device Manager events.
 *
 * @param[in]   p_evt   Data associated to the device manager event.
 */
static uint32_t device_manager_evt_handler(dm_handle_t const * p_handle,
    dm_event_t const * p_event, ret_code_t event_result) {
  APP_ERROR_CHECK(event_result);

  switch (p_event->event_id) {
  case DM_EVT_DEVICE_CONTEXT_LOADED: // Fall through.
  case DM_EVT_SECURITY_SETUP_COMPLETE:
    m_bonded_peer_handle = (*p_handle);
    break;
  }

  return NRF_SUCCESS;
}

/**@brief Function for the Device Manager initialization.
 *
 * @param[in] erase_bonds  Indicates whether bonding information should be cleared from
 *                         persistent storage during initialization of the Device Manager.
 */
static void device_manager_init(bool erase_bonds) {
  uint32_t err_code;
  dm_init_param_t init_param = { .clear_persistent_data = erase_bonds };
  dm_application_param_t register_param;

  // Initialize peer device handle.
  err_code = dm_handle_initialize(&m_bonded_peer_handle);
  APP_ERROR_CHECK(err_code);

  // Initialize persistent storage module.
  err_code = pstorage_init();
  APP_ERROR_CHECK(err_code);

  err_code = dm_init(&init_param);
  APP_ERROR_CHECK(err_code);

  memset(&register_param.sec_param, 0, sizeof(ble_gap_sec_params_t));

  register_param.sec_param.bond = SEC_PARAM_BOND;
  register_param.sec_param.mitm = SEC_PARAM_MITM;
  register_param.sec_param.lesc = SEC_PARAM_LESC;
  register_param.sec_param.keypress = SEC_PARAM_KEYPRESS;
  register_param.sec_param.io_caps = SEC_PARAM_IO_CAPABILITIES;
  register_param.sec_param.oob = SEC_PARAM_OOB;
  register_param.sec_param.min_key_size = SEC_PARAM_MIN_KEY_SIZE;
  register_param.sec_param.max_key_size = SEC_PARAM_MAX_KEY_SIZE;
  register_param.evt_handler = device_manager_evt_handler;
  register_param.service_type = DM_PROTOCOL_CNTXT_GATT_SRVR_ID;

  err_code = dm_register(&m_app_handle, &register_param);
  APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing the Advertising functionality.
 */
static void advertising_init(void) {
  uint32_t err_code;
  ble_advdata_t advdata;

  // Build and set advertising data
  memset(&advdata, 0, sizeof(advdata));

  advdata.name_type = BLE_ADVDATA_FULL_NAME;
  advdata.include_appearance = true;
  advdata.flags = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
  advdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids)
      / sizeof(m_adv_uuids[0]);
  advdata.uuids_complete.p_uuids = m_adv_uuids;

  ble_adv_modes_config_t options = { 0 };
  options.ble_adv_fast_enabled = BLE_ADV_FAST_ENABLED;
  options.ble_adv_fast_interval = APP_ADV_INTERVAL;
  options.ble_adv_fast_timeout = APP_ADV_TIMEOUT_IN_SECONDS;

  err_code = ble_advertising_init(&advdata, NULL, &options, on_adv_evt, NULL);
  APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing buttons and leds.
 *
 * @param[out] p_erase_bonds  Will be true if the clear bonding button was pressed to wake the application up.
 */
static void buttons_leds_init(bool * p_erase_bonds) {
  bsp_event_t startup_event;

  uint32_t err_code = bsp_init(BSP_INIT_LED | BSP_INIT_BUTTONS,
      APP_TIMER_TICKS(100, APP_TIMER_PRESCALER), bsp_event_handler);
  APP_ERROR_CHECK(err_code);

  err_code = bsp_btn_ble_init(NULL, &startup_event);
  APP_ERROR_CHECK(err_code);

  *p_erase_bonds = (startup_event == BSP_EVENT_CLEAR_BONDING_DATA);
}

/**@brief   Function for transmitting a key scan Press & Release Notification.
 *
 * @warning This handler is an example only. You need to analyze how you wish to send the key
 *          release.
 *
 * @param[in]  p_instance     Identifies the service for which Key Notifications are requested.
 * @param[in]  p_key_pattern  Pointer to key pattern.
 * @param[in]  pattern_len    Length of key pattern. 0 < pattern_len < 7.
 * @param[in]  pattern_offset Offset applied to Key Pattern for transmission.
 * @param[out] actual_len     Provides actual length of Key Pattern transmitted, making buffering of
 *                            rest possible if needed.
 * @return     NRF_SUCCESS on success, BLE_ERROR_NO_TX_PACKETS in case transmission could not be
 *             completed due to lack of transmission buffer or other error codes indicating reason
 *             for failure.
 *
 * @note       In case of BLE_ERROR_NO_TX_PACKETS, remaining pattern that could not be transmitted
 *             can be enqueued \ref buffer_enqueue function.
 *             In case a pattern of 'cofFEe' is the p_key_pattern, with pattern_len as 6 and
 *             pattern_offset as 0, the notifications as observed on the peer side would be
 *             1>    'c', 'o', 'f', 'F', 'E', 'e'
 *             2>    -  , 'o', 'f', 'F', 'E', 'e'
 *             3>    -  ,   -, 'f', 'F', 'E', 'e'
 *             4>    -  ,   -,   -, 'F', 'E', 'e'
 *             5>    -  ,   -,   -,   -, 'E', 'e'
 *             6>    -  ,   -,   -,   -,   -, 'e'
 *             7>    -  ,   -,   -,   -,   -,  -
 *             Here, '-' refers to release, 'c' refers to the key character being transmitted.
 *             Therefore 7 notifications will be sent.
 *             In case an offset of 4 was provided, the pattern notifications sent will be from 5-7
 *             will be transmitted.
 */
static uint32_t send_key_scan_press_release(ble_hids_t * p_hids,
    uint8_t * p_key_pattern, uint16_t pattern_len, uint16_t pattern_offset,
    uint16_t * p_actual_len) {
  uint32_t err_code;
  uint16_t offset;
  uint16_t data_len;
  uint8_t data[INPUT_REPORT_KEYS_MAX_LEN];

  // HID Report Descriptor enumerates an array of size 6, the pattern hence shall not be any
  // longer than this.
  STATIC_ASSERT((INPUT_REPORT_KEYS_MAX_LEN - 2) == 6);

  ASSERT(pattern_len <= (INPUT_REPORT_KEYS_MAX_LEN - 2));

  offset = pattern_offset;
  data_len = pattern_len;

  do {
    // Reset the data buffer.
    memset(data, 0, sizeof(data));

    // Copy the scan code.
    memcpy(data + SCAN_CODE_POS + offset, p_key_pattern + offset,
        data_len - offset);

    if (!m_in_boot_mode) {
      err_code = ble_hids_inp_rep_send(p_hids,
      INPUT_REPORT_KEYS_INDEX,
      INPUT_REPORT_KEYS_MAX_LEN, data);
    } else {
      err_code = ble_hids_boot_kb_inp_rep_send(p_hids,
      INPUT_REPORT_KEYS_MAX_LEN, data);
    }

    if (err_code != NRF_SUCCESS) {
      break;
    }

    offset++;
  } while (offset <= data_len);

  *p_actual_len = offset;

  return err_code;
}

/**@brief Function for sending sample key presses to the peer.
 *
 * @param[in]   key_pattern_len   Pattern length.
 * @param[in]   p_key_pattern     Pattern to be sent.
 */
static void keys_send(uint8_t key_pattern_len, uint8_t * p_key_pattern) {
  uint32_t err_code;
  uint16_t actual_len;

  err_code = send_key_scan_press_release(&m_hids, p_key_pattern,
      key_pattern_len, 0, &actual_len);
  // An additional notification is needed for release of all keys, therefore check
  // is for actual_len <= key_pattern_len and not actual_len < key_pattern_len.
  if ((err_code == BLE_ERROR_NO_TX_PACKETS)
      && (actual_len <= key_pattern_len)) {
    // Buffer enqueue routine return value is not intentionally checked.
    // Rationale: Its better to have a a few keys missing than have a system
    // reset. Recommendation is to work out most optimal value for
    // MAX_BUFFER_ENTRIES to minimize chances of buffer queue full condition
    //buffer_enqueue(&m_hids, p_key_pattern, key_pattern_len, actual_len);
    // TODO add keypress to queue
  }

  if ((err_code != NRF_SUCCESS) && (err_code != NRF_ERROR_INVALID_STATE)
      && (err_code != BLE_ERROR_NO_TX_PACKETS)
      && (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING)) {
    APP_ERROR_HANDLER(err_code);
  }
}

/**@brief Function for handling the HID Report Characteristic Write event.
 *
 * @param[in]   p_evt   HID service event.
 */
static void on_hid_rep_char_write(ble_hids_evt_t *p_evt) {
  if (p_evt->params.char_write.char_id.rep_type == BLE_HIDS_REP_TYPE_OUTPUT) {
    uint32_t err_code;
    uint8_t report_val;
    uint8_t report_index = p_evt->params.char_write.char_id.rep_index;

    if (report_index == OUTPUT_REPORT_INDEX) {
      // This code assumes that the output report is one byte long. Hence the following
      // static assert is made.
      STATIC_ASSERT(OUTPUT_REPORT_MAX_LEN == 1);

      err_code = ble_hids_outp_rep_get(&m_hids, report_index,
      OUTPUT_REPORT_MAX_LEN, 0, &report_val);
      APP_ERROR_CHECK(err_code);

      if (!m_caps_on
          && ((report_val & OUTPUT_REPORT_BIT_MASK_CAPS_LOCK) != 0)) {
        // Caps Lock is turned On.
        // TODO turn on caps lock light

        keys_send(sizeof(MANUFACTURER_NAME), (uint8_t *)MANUFACTURER_NAME);
        m_caps_on = true;
      } else if (m_caps_on
          && ((report_val & OUTPUT_REPORT_BIT_MASK_CAPS_LOCK) == 0)) {
        // Caps Lock is turned Off .
        err_code = bsp_indication_set(BSP_INDICATE_ALERT_OFF);
        APP_ERROR_CHECK(err_code);

        keys_send(sizeof(MANUFACTURER_NAME), (uint8_t *)MANUFACTURER_NAME);
        m_caps_on = false;
      } else {
        // The report received is not supported by this application. Do nothing.
      }
    }
  }
}

/**@brief Function for handling HID events.
 *
 * @details This function will be called for all HID events which are passed to the application.
 *
 * @param[in]   p_hids  HID service structure.
 * @param[in]   p_evt   Event received from the HID service.
 */
static void on_hids_evt(ble_hids_t * p_hids, ble_hids_evt_t *p_evt) {
  switch (p_evt->evt_type) {
  case BLE_HIDS_EVT_BOOT_MODE_ENTERED:
    m_in_boot_mode = true;
    break;

  case BLE_HIDS_EVT_REPORT_MODE_ENTERED:
    m_in_boot_mode = false;
    break;

  case BLE_HIDS_EVT_REP_CHAR_WRITE:
    on_hid_rep_char_write(p_evt);
    break;

  case BLE_HIDS_EVT_NOTIF_ENABLED: {
    dm_service_context_t service_context;
    service_context.service_type = DM_PROTOCOL_CNTXT_GATT_SRVR_ID;
    service_context.context_data.len = 0;
    service_context.context_data.p_data = NULL;

    if (m_in_boot_mode) {
      // Protocol mode is Boot Protocol mode.
      if (p_evt->params.notification.char_id.uuid ==
      BLE_UUID_BOOT_KEYBOARD_INPUT_REPORT_CHAR) {
        // The notification of boot keyboard input report has been enabled.
        // Save the system attribute (CCCD) information into the flash.
        uint32_t err_code;

        err_code = dm_service_context_set(&m_bonded_peer_handle,
            &service_context);
        if (err_code != NRF_ERROR_INVALID_STATE) {
          APP_ERROR_CHECK(err_code);
        } else {
          // The system attributes could not be written to the flash because
          // the connected central is not a new central. The system attributes
          // will only be written to flash only when disconnected from this central.
          // Do nothing now.
        }
      } else {
        // Do nothing.
      }
    } else if (p_evt->params.notification.char_id.rep_type
        == BLE_HIDS_REP_TYPE_INPUT) {
      // The protocol mode is Report Protocol mode. And the CCCD for the input report
      // is changed. It is now time to store all the CCCD information (system
      // attributes) into the flash.
      uint32_t err_code;

      err_code = dm_service_context_set(&m_bonded_peer_handle,
          &service_context);
      if (err_code != NRF_ERROR_INVALID_STATE) {
        APP_ERROR_CHECK(err_code);
      } else {
        // The system attributes could not be written to the flash because
        // the connected central is not a new central. The system attributes
        // will only be written to flash only when disconnected from this central.
        // Do nothing now.
      }
    } else {
      // The notification of the report that was enabled by the central is not interesting
      // to this application. So do nothing.
    }
    break;
  }

  default:
    // No implementation needed.
    break;
  }
}

/**@brief Function for initializing HID Service.
 */
static void hids_init(void) {
  uint32_t err_code;
  ble_hids_init_t hids_init_obj;
  ble_hids_inp_rep_init_t input_report_array[1];
  ble_hids_inp_rep_init_t * p_input_report;
  ble_hids_outp_rep_init_t output_report_array[1];
  ble_hids_outp_rep_init_t * p_output_report;
  uint8_t hid_info_flags;

  memset((void *) input_report_array, 0, sizeof(ble_hids_inp_rep_init_t));
  memset((void *) output_report_array, 0, sizeof(ble_hids_outp_rep_init_t));

  static uint8_t report_map_data[] = { 0x05, 0x01, // Usage Page (Generic Desktop)
      0x09, 0x06,                 // Usage (Keyboard)
      0xA1, 0x01,                 // Collection (Application)
      0x05, 0x07,                 //     Usage Page (Key Codes)
      0x19, 0xe0,                 //     Usage Minimum (224)
      0x29, 0xe7,                 //     Usage Maximum (231)
      0x15, 0x00,                 //     Logical Minimum (0)
      0x25, 0x01,                 //     Logical Maximum (1)
      0x75, 0x01,                 //     Report Size (1)
      0x95, 0x08,                 //     Report Count (8)
      0x81, 0x02,                 //     Input (Data, Variable, Absolute)

      0x95, 0x01,                 //     Report Count (1)
      0x75, 0x08,                 //     Report Size (8)
      0x81, 0x01,                 //     Input (Constant) reserved byte(1)

      0x95, 0x05,                 //     Report Count (5)
      0x75, 0x01,                 //     Report Size (1)
      0x05, 0x08,                 //     Usage Page (Page# for LEDs)
      0x19, 0x01,                 //     Usage Minimum (1)
      0x29, 0x05,                 //     Usage Maximum (5)
      0x91, 0x02,           //     Output (Data, Variable, Absolute), Led report
      0x95, 0x01,                 //     Report Count (1)
      0x75, 0x03,                 //     Report Size (3)
      0x91, 0x01,   //     Output (Data, Variable, Absolute), Led report padding

      0x95, 0x06,                 //     Report Count (6)
      0x75, 0x08,                 //     Report Size (8)
      0x15, 0x00,                 //     Logical Minimum (0)
      0x25, 0x65,                 //     Logical Maximum (101)
      0x05, 0x07,                 //     Usage Page (Key codes)
      0x19, 0x00,                 //     Usage Minimum (0)
      0x29, 0x65,                 //     Usage Maximum (101)
      0x81, 0x00,                 //     Input (Data, Array) Key array(6 bytes)

      0x09, 0x05,                 //     Usage (Vendor Defined)
      0x15, 0x00,                 //     Logical Minimum (0)
      0x26, 0xFF, 0x00,           //     Logical Maximum (255)
      0x75, 0x08,                 //     Report Count (2)
      0x95, 0x02,                 //     Report Size (8 bit)
      0xB1, 0x02,                 //     Feature (Data, Variable, Absolute)

      0xC0                        // End Collection (Application)
      };

  // Initialize HID Service
  p_input_report = &input_report_array[INPUT_REPORT_KEYS_INDEX];
  p_input_report->max_len = INPUT_REPORT_KEYS_MAX_LEN;
  p_input_report->rep_ref.report_id = INPUT_REP_REF_ID;
  p_input_report->rep_ref.report_type = BLE_HIDS_REP_TYPE_INPUT;

  BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(
      &p_input_report->security_mode.cccd_write_perm);
  BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(
      &p_input_report->security_mode.read_perm);
  BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(
      &p_input_report->security_mode.write_perm);

  p_output_report = &output_report_array[OUTPUT_REPORT_INDEX];
  p_output_report->max_len = OUTPUT_REPORT_MAX_LEN;
  p_output_report->rep_ref.report_id = OUTPUT_REP_REF_ID;
  p_output_report->rep_ref.report_type = BLE_HIDS_REP_TYPE_OUTPUT;

  BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(
      &p_output_report->security_mode.read_perm);
  BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(
      &p_output_report->security_mode.write_perm);

  hid_info_flags = HID_INFO_FLAG_REMOTE_WAKE_MSK
      | HID_INFO_FLAG_NORMALLY_CONNECTABLE_MSK;

  memset(&hids_init_obj, 0, sizeof(hids_init_obj));

  hids_init_obj.evt_handler = on_hids_evt;
  hids_init_obj.error_handler = error_handler;
  hids_init_obj.is_kb = true;
  hids_init_obj.is_mouse = false;
  hids_init_obj.inp_rep_count = 1;
  hids_init_obj.p_inp_rep_array = input_report_array;
  hids_init_obj.outp_rep_count = 1;
  hids_init_obj.p_outp_rep_array = output_report_array;
  hids_init_obj.feature_rep_count = 0;
  hids_init_obj.p_feature_rep_array = NULL;
  hids_init_obj.rep_map.data_len = sizeof(report_map_data);
  hids_init_obj.rep_map.p_data = report_map_data;
  hids_init_obj.hid_information.bcd_hid = BASE_USB_HID_SPEC_VERSION;
  hids_init_obj.hid_information.b_country_code = 0;
  hids_init_obj.hid_information.flags = hid_info_flags;
  hids_init_obj.included_services_count = 0;
  hids_init_obj.p_included_services_array = NULL;

  BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(
      &hids_init_obj.rep_map.security_mode.read_perm);
  BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(
      &hids_init_obj.rep_map.security_mode.write_perm);
  BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(
      &hids_init_obj.hid_information.security_mode.read_perm);
  BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(
      &hids_init_obj.hid_information.security_mode.write_perm);

  BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(
      &hids_init_obj.security_mode_boot_kb_inp_rep.cccd_write_perm);
  BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(
      &hids_init_obj.security_mode_boot_kb_inp_rep.read_perm);
  BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(
      &hids_init_obj.security_mode_boot_kb_inp_rep.write_perm);
  BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(
      &hids_init_obj.security_mode_boot_kb_outp_rep.read_perm);
  BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(
      &hids_init_obj.security_mode_boot_kb_outp_rep.write_perm);

  BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(
      &hids_init_obj.security_mode_protocol.read_perm);
  BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(
      &hids_init_obj.security_mode_protocol.write_perm);
  BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(
      &hids_init_obj.security_mode_ctrl_point.read_perm);
  BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(
      &hids_init_obj.security_mode_ctrl_point.write_perm);

  err_code = ble_hids_init(&m_hids, &hids_init_obj);
  APP_ERROR_CHECK(err_code);
}

/**@brief Thread for handling the Application's BLE Stack events.
 *
 * @details This thread is responsible for handling BLE Stack events sent from on_ble_evt().
 *
 * @param[in]   arg   Pointer used for passing some arbitrary information (context) from the
 *                    osThreadCreate() call to the thread.
 */
static void ble_stack_thread(void * arg) {
  uint32_t err_code;
  bool erase_bonds;

  UNUSED_PARAMETER(arg);

  // Initialize.
  timers_init();
  buttons_leds_init(&erase_bonds);
  ble_stack_init();
  device_manager_init(erase_bonds);
  gap_params_init();
  advertising_init();
  services_init();
  conn_params_init();

  application_timers_start();
  err_code = ble_advertising_start(BLE_ADV_MODE_FAST);
  APP_ERROR_CHECK(err_code);

  while (1) {
    /* Wait for event from SoftDevice */
    while (pdFALSE == xSemaphoreTake(m_ble_event_ready, portMAX_DELAY)) {
      // Just wait again in the case when INCLUDE_vTaskSuspend is not enabled
    }

    // This function gets events from the SoftDevice and processes them by calling the function
    // registered by softdevice_ble_evt_handler_set during stack initialization.
    // In this code ble_evt_dispatch would be called for every event found.
    intern_softdevice_events_execute();
  }
}

/**@brief Function for application main entry.
 */
int main(void) {
  // Do not start any interrupt that uses system functions before system initialisation.
  // The best solution is to start the OS before any other initalisation.

  // Init a semaphore for the BLE thread.
  m_ble_event_ready = xSemaphoreCreateBinary();
  if (NULL == m_ble_event_ready) {
    APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
  }

  // Create BLE thread
  if (pdPASS
      != xTaskCreate(ble_stack_thread, "BLE", 256, NULL, 1, &m_ble_stack_thread)) {
    APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
  }

  // Start other tasks
  key_scan_init();
  led_driver_init();

  /* Activate deep sleep mode */
  SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;

  // Start FreeRTOS scheduler.
  vTaskStartScheduler();

  while (true) {
    APP_ERROR_HANDLER(NRF_ERROR_FORBIDDEN);
  }
}
