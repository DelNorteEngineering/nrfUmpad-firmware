/*
 * keyboard_scan.c
 *
 *  Created on: Jul 18, 2016
 *      Author: nolan
 */

#include "key_scan.h"
#include "FreeRTOS.h"
#include "task.h"
#include "app_error.h"

// static vars
static xTaskHandle key_scan_task = NULL;

// static prototypes
static void task(void *args);
static void key_isr();

void key_scan_init() {
  APP_ERROR_CHECK_BOOL(
      xTaskCreate(task, "key_scan", 128, NULL, KEY_SCAN_PRIORITY, &key_scan_task));

  // TODO set up gpio for keypad
}

static void task(void *args) {

  // TODO set up isr for keypress and semaphore
  (void)key_isr();

  for (;;) {
    // TODO take semaphore
    // TODO scan keys and notify BLE thread
  }

}

static void key_isr() {
  // TODO raise semaphore to trigger key scan
}
