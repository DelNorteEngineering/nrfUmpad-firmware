/*
 * led_driver.cpp
 *
 *  Created on: Jul 18, 2016
 *      Author: nolan
 */

#include "led_driver.h"

#include "app_error.h"
#include "FreeRTOS.h"
#include "task.h"

// static prototypes
static void task(void *args);
static void reset();

static xTaskHandle led_driver_task = NULL;

void led_driver_init() {
  APP_ERROR_CHECK_BOOL(
      xTaskCreate(task, "led_driver", 64, NULL, LED_DRIVER_TASK_PRIORITY, &led_driver_task));

  // TODO set up i2c bus
}

static void task(void *args) {
  // TODO set up notification queue
  reset();

  for (;;) {
    // TODO watch queue for notifications
    // TODO set LEDs accordingly
  }
}

static void reset() {
  // TODO reset LED driver driver
}

