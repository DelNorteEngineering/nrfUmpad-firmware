/*
 * led_driver.c
 *
 *  Created on: Jul 18, 2016
 *      Author: nolan
 */

#ifndef INCLUDE_LED_DRIVER_C_
#define INCLUDE_LED_DRIVER_C_

#define LED_DRIVER_TASK_PRIORITY 4

// prototypes
void led_driver_init();

#endif /* INCLUDE_LED_DRIVER_C_ */
