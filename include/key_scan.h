/*
 * keyboard_scan.h
 *
 *  Created on: Jul 18, 2016
 *      Author: nolan
 */

#ifndef INCLUDE_KEY_SCAN_H_
#define INCLUDE_KEY_SCAN_H_

#define KEY_SCAN_PRIORITY 2

void key_scan_init();

#endif /* INCLUDE_KEY_SCAN_H_ */
